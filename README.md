![Blumon Pay Magento](https://blumonpay.com/images/bp_blue_l.png)

# Requirements

PHP 5.4 or greater


# Supported Version
- Magento Community Edition 1.9.4.4


# Installation

- Option A (Recommended)

1. Download latest [.tgz file]
2. In your admin go to **System > Magento Connect > Magento Connect Manager** 
3. Update the .tgz file using **Direct package file upload**

- Option B

1. Copy the folders **app** to the Magento root installation. Make sure to keep the Magento folders structure intact.
2. In your admin go to **System > Cache Management** and clear all caches.
3. Go to **System > IndexManagement** and select all fields. Then click in Reindex Data.


# Configuration

1. Go to **System > Configuration > Sales > Payment Methods**. Select Blumon Pay Charges.

2. Configure the title that will appear in the payment module (Title)

3. Enable the payment module (Payment Enabled)

4. Sandbox mode for testing your module. (Sandbox Mode)

![](https://blumonpay.com/images/activate.jpg)

5. Set your API USERNAME, API PASSWORD (SHA-256) (Get these values with the Blumon Pay team).

![](https://blumonpay.com/images/credentials.jpg)

6. Enable the payments methods that you want to have

![](https://blumonpay.com/images/methods.jpg)

<?php

class BlumonPay_Charges_Model_Paymentmethod extends Mage_Payment_Model_Method_Cc {

  protected $_code  = 'charges';
  protected $_blumonpay;
  protected $_canSaveCc = false;
  protected $_canRefund = true;
  protected $_isGateway = true;
  protected $_canAuthorize = true;
  protected $_canCapture = true;
  protected $_canCapturePartial = true;
  protected $_canVoid = true;
  protected $_canRefundInvoicePartial = true;
  protected $_formBlockType = 'charges/form_charges';
  protected $_infoBlockType = 'charges/info_charges';
 
  public function assignData($data)
  {
    $paymentRequest = Mage::app()->getRequest()->getPost('payment');

    $info = $this->getInfoInstance();

    $info->setMomentumToken($paymentRequest['momentum_token']);

    return parent::assignData($data);
  }

  public function prepareSave() {
    $info = $this->getInfoInstance();
    if ($this->_canSaveCc) {
      $info->setCcNumberEnc($info->encrypt($info->getCcNumber()));
    }

    return $this;
  }

  public function validate()
  {

    $info = $this->getInfoInstance();
    $errorMsg = false;
    $availableTypes = explode(',', $this->getConfigData('cctypes'));

    if ($info->getOpenpayCC() != 'new') {
      return $this;
    }

    if (!in_array($info->getCcType(), $availableTypes)) {
      $errorMsg = Mage::helper('payment')->__('Credit card type is not allowed for this payment method.');
    }

    if ($info->getCcExpYear() <> null || $info->getCcExpMonth() <> null || $info->getCcCidEnc() <> null) {
      $errorMsg = Mage::helper('payment')->__('Your checkout form is sending sensitive information to the server. Please contact your developer to fix this security leak.');
    }

    $ccNumber = $info->getCcNumber();
    if ($info->getCcType() == 'CN' && !preg_match('/^[0-9]{16}$/', $ccNumber)) {
      Mage::throwException($this->_getHelper()->__('Credit card number mismatch with credit card type.'));
    }

    $verificationRegex = $this->getVerificationRegEx();
    if (!preg_match($verificationRegex[$info->getCcType()], $info->getCcCid())) {            
      $errorMsg = Mage::helper('payment')->__('Please enter a valid credit card verification number.');
    }

    if ($errorMsg) {
      Mage::log('#ERROR validate() => '.$errorMsg);
      Mage::throwException($errorMsg);
    }
        
    return $this;
  }

  public function authorize(Varien_Object $payment, $amount) {
    $data = $payment->getData();
    print_r($data);

    return $this;
  }

  public function capture(Varien_Object $payment, $amount) {

    $order = $payment->getOrder();
    $result = $this->callApi($payment, $amount, 'authorize');
    if($result === false) {
      $errorCode = 'Invalid Data';
      $errorMsg = $this->_getHelper()->__('Error Processing the request');
    } else {
      Mage::log($result, null, $this->getCode().'.log');

      if($result['status'] == 1){
        $payment->setTransactionId($result['transaction_id']);
        $payment->setIsTransactionClosed(0);
        $payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,array('key1'=>'value1','key2'=>'value2')); //use this in case you want to add some extra information
      }else{

        if($result['response'] != null){
          $errorMsg = $result['response']->error->description;
        } else {
          $errorMsg = $this->_getHelper()->__('Error Processing the request');
        }
        
        Mage::throwException($errorMsg);
      }

      // Add the comment and save the order
    } if($errorMsg) {
      Mage::throwException($errorMsg);
    }
    

    return $this;
  }

  public function void(Varien_Object $payment) {
    parent::void($payment);

    return $this;
  }

  public function refund(Varien_Object $payment) {
    parent::refund($payment);

    return $this;
  }

  private function callApi(Varien_Object $payment, $amount, $type){
    $info = $this->getInfoInstance();
    $paymentRequest = Mage::app()->getRequest()->getPost('payment');
    $info->setMomentumToken($paymentRequest['momentum_token']);
    $statusTrx = false;

    $curl = curl_init(); 

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->getMomentumUrl().'/ecommerce/charge',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS =>'{
        "amount": "'.$amount.'",
        "currency": "484",
        "noPresentCardData": {
            "cardNumber": "'.$payment->getCcNumber().'",
            "cvv": "'.$payment->getCcCid().'",
            "cardholderName": "'.$payment->getCcOwner().'",
            "expirationYear": "'.substr($payment->getCcExpYear(), -2).'",
            "expirationMonth": "'.str_pad($payment->getCcExpMonth(),2,"0",STR_PAD_LEFT).'"
        }
    }',
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer '.$info->getMomentumToken(),
        'Content-Type: application/json'
      ),
      CURLOPT_SSL_VERIFYHOST => 0,
      CURLOPT_SSL_VERIFYPEER => 0,
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    Mage::log('TRANSACTION CARD RESPONSE:', null, 'charges.log', true);
    Mage::log($response, null, 'charges.log', true);

    $response = json_decode($response);

    if (isset($response->status) && $response->status) {
      $statusTrx = $response->status;
    }

    if (!isset($response->status) && isset($response->error)) {
      Mage::throwException($response->error);
    }

    if (!isset($response->status) && !isset($response->error)) {
      Mage::throwException('Lo sentimos, pero no podemos procesar su solicitud. Inténtelo de nuevo más tarde.');
    }

    return array('status'=>$statusTrx,'transaction_id' => time() , 'fraud' => rand(0,1), 'response'=> $response);
  }

  protected function _getHelper() {
    return Mage::helper('charges');
  }

  public function getVerificationRegEx() {
    return array_merge(parent::getVerificationRegEx(), array('CN' => '/^[0-9]{3}$/'));
  }

  protected function getSandboxMode() {
      return Mage::getStoreConfig('payment/charges/test');
  }

  protected function getMomentumUrl() {
      if ($this->getSandboxMode() == 1) {
          return "https://sandbox-ecommerce.blumonpay.net";
      } else {
          return "https://e-commerce.blumonpay.net";
      }
  }
}




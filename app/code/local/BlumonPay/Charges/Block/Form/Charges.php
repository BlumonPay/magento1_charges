<?php

class BlumonPay_Charges_Block_Form_Charges extends Mage_Payment_Block_Form_Cc {
    
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('charges/form/charges.phtml');
    }

    protected function getAuthToken() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->getMomentumUrl().'/oauth/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'grant_type=password&username='.$this->getAPIUser().'&password='.$this->getAPIPass(),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization: Basic Ymx1bW9uX3BheV9lY29tbWVyY2VfYXBpOmJsdW1vbl9wYXlfZWNvbW1lcmNlX2FwaV9wYXNzd29yZA=='
            ),
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        ));
        $response = curl_exec($curl);

        curl_close($curl);
        $json_obj = json_decode($response);

        return $json_obj->access_token;
    }

    protected function getSandboxMode() {
        return Mage::getStoreConfig('payment/charges/test');
    }

    protected function getMomentumUrl() {
        if ($this->getSandboxMode() == 1) {
            return "https://sandbox-tokener.blumonpay.net";
        } else {
            return "https://tokener.blumonpay.net";
        }
    }

    protected function getAPIUser() {
        $encryptedApiUser = Mage::getStoreConfig('payment/charges/login');
        $apiUser = Mage::helper('core')->decrypt($encryptedApiUser);
        return $apiUser;
    }

    protected function getAPIPass() {
        $encryptedApiUser = Mage::getStoreConfig('payment/charges/trans_key');
        $apiUser = Mage::helper('core')->decrypt($encryptedApiUser);
        return $apiUser;
    }
}

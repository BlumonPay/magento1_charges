<?php

class BlumonPay_Charges_Block_Info_Charges extends Mage_Payment_Block_Info_Cc {

  protected function _prepareSpecificInformation($transport = null)
  {
    if (null !== $this->_paymentSpecificInformation) 
    {
      return $this->_paymentSpecificInformation;
    }

    $data = array();

    $data[Mage::helper('payment')->__('Total Amount')] = '$'.$this->helper('checkout/cart')->getQuote()->getGrandTotal();
    $data[Mage::helper('payment')->__('Holder Name')] = $this->getInfo()->getCcOwner();
 
    $transport = parent::_prepareSpecificInformation($transport);
     
    return $transport->setData(array_merge($data, $transport->getData()));
  }
}
<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('sales/quote_payment'), 'blumonpay_token', array(
    'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
    'comment' => 'BlumonPay Token',
    'length'  => '255'
));

$installer->getConnection()->addColumn($installer->getTable('sales/order_payment'), 'blumonpay_authorization', array(
    'type'    => Varien_Db_Ddl_Table::TYPE_NUMERIC,
    'comment' => 'BlumonPay Authorization Code',
    'length' => 10
));

$installer->getConnection()->addColumn($installer->getTable('sales/order_payment'), 'blumonpay_creation_date', array(
    'type'    => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    'comment' => 'BlumonPay Transaction Date'
));

$installer->getConnection()->addColumn($installer->getTable('sales/order_payment'), 'blumonpay_payment_id', array(
    'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
    'comment' => 'BlumonPay Payment Id',
    'length'  => '255'
));

$installer->endSetup();